const image1 = new Image();
image1.src = 'kaidan.jpg';


image1.addEventListener('load', function(){
    const canvas = document.getElementById('canvas1');
    const context = canvas.getContext('2d');
    canvas.width = 500;
    canvas.height = 730;
    
    context.drawImage(image1, 0, 0, canvas.width, canvas.height);

    //gets the data for every pixel of the image
    const pixels = context.getImageData(0, 0, canvas.width, canvas.height);
    //deletes the image (jpg) from canvas after scanning it for data
    context.clearRect(0, 0, canvas.width, canvas.height);

    let mappedImage = [];
    for (let y = 0; y < canvas.height; y++){
        let row = [];
        for(let x = 0; x <canvas.width; x++){
            const red = pixels.data[(y * 4 * pixels.width) + (x * 4)];
            const green = pixels.data[(y * 4 * pixels.width) + (x * 4 + 1)];
            const blue = pixels.data[(y * 4 * pixels.width) + (x * 4 + 2)];
            const brightness = calculateRelativeBrightness(red, green, blue);
            const cell = [
                cellBrightness = brightness,
            ];
            row.push(cell);
        }
        mappedImage.push(row);
    }

    function calculateRelativeBrightness(red, green, blue){
        return Math.sqrt(
            (red * red) * 0.299 +
            (green * green) * 0.587 +
            (blue * blue) * 0.114
        )/100;
    }

    let particlesArray = [];
    //density of the effect
    const numberOfParticles = 5000;

    class Particle {
        constructor(){
            this.x = Math.random() * canvas.width;
            this.y = 0;
            this.speed = 0;
            this.velocity = Math.random() * 0.5;
            this.size = Math.random() * 1.5 + 1;
            this.position1 = Math.floor(this.y);
            this.position2 = Math.floor(this.x);
        }
        //updates the position of the particle
        update(){
            this.position1 = Math.floor(this.y);
            this.position2 = Math.floor(this.x);
            //calculates the speed of particles based on brightness
            this.speed = mappedImage[this.position1][this.position2][0];
            //movement depends on the brighness of the image,
            //also multiplying by velocity ensures the randomnes in speed
            //of the particles in the same brighness areas
            let movement = (2.5 - this.speed) + this.velocity;

            this.y+= movement;
            if(this.y >= canvas.height){
                this.y = 0;
                this.x = Math.random() * canvas.width;
            }
        }
        //draws the particle as an circle
        draw(){
            context.beginPath();
            context.fillStyle = 'white';
            context.arc(this.x, this.y, this.size, 0, Math.PI * 2);
            context.fill()
        }
    }
    function init(){
        for(let i = 0; i < numberOfParticles; i++){
            particlesArray.push(new Particle);
        }
    }
    init();
    function animate(){
        context.globalAlpha = 0.05;
        context.fillStyle = 'rgb(0, 0, 0)';
        context.fillRect(0, 0, canvas.width, canvas.height);
        context.globalAlpha = 0.2;
        for(let i = 0; i < particlesArray.length; i++){
            particlesArray[i].update();
            context.globalAlpha = particlesArray[i].speed * 0.3;
            particlesArray[i].draw();
        }
        requestAnimationFrame(animate);
    }
    animate();
});