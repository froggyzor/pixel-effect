const canvas = document.getElementById('canvas1');
//context - or the 'way' of rendering the image
const ctx = canvas.getContext('2d');

//size of the canvas
canvas.width = 800;
canvas.height = 450;

//image
const image1 = new Image();
//image path
image1.src = 'zaba.jpg';
//in case of cross origin problem use dataURL of the image instead of image itself

image1.addEventListener('load', function(){
    //draw the image on the canvas
    //1st param is the image, 2nd and 3rd are the starting coords for drawing
    ctx.drawImage(image1, 0, 0, canvas.width, canvas.height);

    //scans each pixel in the image from point 0,0 to 800,450
    const scannedImage = ctx.getImageData(0, 0, canvas.width, canvas.height);
    console.log(scannedImage);

    //gets the array from the ImageData object
    const scannedData = scannedImage.data;

    //needs to skip the loop by fours, because of the pixel representation in the array. Each pixel is represented by four numbers (r, g, b, alpha) from 0-255
    for (let i = 0; i < scannedData.length; i += 4){

        //gets the 'whole pixel', its all parts (red, green, blue)
        const total = scannedData[i] + scannedData[i+1] + scannedData[i+2];

        //intensity of the greyscall
        const averageColorValue = total/3;
        scannedData[i] = averageColorValue;
        scannedData[i+1] = averageColorValue;
        scannedData[i+2] = averageColorValue;
    }

    scannedImage.data = scannedData;
    ctx.putImageData(scannedImage, 0, 0);
})